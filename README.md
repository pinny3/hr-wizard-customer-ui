##Test locally

    $ npm install -g browser-sync

    $ browser-sync start --server
    
## Bootstrap docu at

    $ cd $HOME/Projekte/zwl/bootstrap-3.4.1
    
    $ bundle exec jekyll serve

## CDK Toolkit

### Install CDK

    $ npm install -g aws-cdk

### Tasks

The [`cdk.json`](./cdk.json) file in the root of this repository includes
instructions for the CDK toolkit on how to execute this program.

After building your TypeScript code, you will be able to run the CDK toolkits commands as usual:

    $ cdk bootstrap --profile hrw

    $ cdk ls
    <list all stacks in this program>

    $ cdk synth --profile hrw
    <generates and outputs cloudformation template>

    $ cdk deploy --profile hrw
    <deploys stack to your account>

    $ cdk diff --profile hrw
    <shows diff against deployed stack>