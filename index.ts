import s3 = require('@aws-cdk/aws-s3');
import cdk = require('@aws-cdk/core');
import {RemovalPolicy} from "@aws-cdk/core";
import s3deploy = require('@aws-cdk/aws-s3-deployment');
import route53 = require('@aws-cdk/aws-route53');
import acm = require('@aws-cdk/aws-certificatemanager');
import cloudfront = require('@aws-cdk/aws-cloudfront');
import targets = require('@aws-cdk/aws-route53-targets/lib');

class HRWCustomerUI extends cdk.Stack {
    constructor(scope: cdk.App, id: string, props?: cdk.StackProps) {
        super(scope, id, props);

        const route53Zone = new route53.HostedZone(this, 'route53HRW', {
            zoneName: 'customer.hr-wizard.de'
        });

        const siteDomain = 'job.customer.hr-wizard.de';
        new cdk.CfnOutput(this, 'Site', { value: 'https://' + siteDomain });

        const certificateArn = new acm.DnsValidatedCertificate(this, 'SiteCertificate', {
             domainName: siteDomain,
             hostedZone: route53Zone
         }).certificateArn;
        new cdk.CfnOutput(this, 'Certificate', { value: certificateArn });

         const bucketHrw = new s3.Bucket(this, 'bucketHRW', {
                 bucketName: siteDomain,
                 publicReadAccess: true,
                 websiteIndexDocument: 'index.html',
                 websiteErrorDocument: 'error.html',
                 removalPolicy: RemovalPolicy.DESTROY
             }
         );
         new cdk.CfnOutput(this, 'Bucket', { value: bucketHrw.bucketName });

        // CloudFront distribution that provides HTTPS
        const distribution = new cloudfront.CloudFrontWebDistribution(this, 'SiteDistribution', {
            aliasConfiguration: {
                acmCertRef: certificateArn,
                names: [ siteDomain ],
                sslMethod: cloudfront.SSLMethod.SNI,
                securityPolicy: cloudfront.SecurityPolicyProtocol.TLS_V1_1_2016,
            },
            originConfigs: [
                {
                    s3OriginSource: {
                        s3BucketSource: bucketHrw
                    },
                    behaviors : [ {isDefaultBehavior: true}],
                }
            ]
        });
        new cdk.CfnOutput(this, 'DistributionId', { value: distribution.distributionId });

        new route53.ARecord(this, 'SiteAliasRecord', {
            recordName: siteDomain,
            target: route53.RecordTarget.fromAlias(new targets.CloudFrontTarget(distribution)),
            zone: route53Zone
        });

        new s3deploy.BucketDeployment(this, 'DeployWithInvalidation', {
             sources: [ s3deploy.Source.asset('./predeploy/src') ],
             destinationBucket: bucketHrw,
             distribution,
             distributionPaths: ['/*'],
        });
    }
}

const app = new cdk.App();

new HRWCustomerUI(app, 'HRWCustomerUI', {
    env: {
        'account': '921717420304',
        'region': 'us-east-1'
    }
});
app.synth();
