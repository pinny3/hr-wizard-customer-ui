const callQuarkus = async _ => {
	const response = await fetch("http://localhost:7070/hello");
	const message = await response.text();
	console.log(`${message} from quarkus`);
};

callQuarkus();
