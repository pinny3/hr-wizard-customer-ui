class Apply {

    constructor() {
        this.documents = [
            new Document('Anschreiben', 'letter', '*'),
            new Document('Lebenslauf', 'cv', '*'),
            new Document('Bild', 'image', '*'),
            new Document('Video', 'video', '*'),
            new Document('Link', 'link', '*'),
        ];
        this.selectedDocuments = [this.documents[0]];
    }

    updateSlot = (content) => {
        this.slot = document.querySelector('#documents');
        this.slot.insertAdjacentHTML( 'beforeend', content );
    };

    removeVideo() {
        document.getElementById("video").value="";
    }

}

class Document {

    constructor(displayName, name, mediatype) {
        this.displayName = displayName;
        this.name = name;
        this.mediatype = mediatype;
    }

}

const apply = new Apply();

