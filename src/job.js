class Job {

    constructor() {
        this.jobId = '';
        this.title = '';
        this.description = '';
        this.descriptionPdfLink = '';
        this.jobSkills = [];
        this.jobRef = '';
        this.applicationId = '';
        this.gdprRef = '';
        this.salutation = '';
        this.lastName = '';
    }

    loadJob = async (parameterName) => {
        this.jobRef = this.getParameterByName(parameterName);
        console.log(`Trying to fetch job with id ${this.jobRef}`);
        const response = await fetch(`http://localhost:7070/job/1/${this.jobRef}`);
        if (response.status === 200) {
            const job = await response.json();
            console.log(`Fetched job with id ${job.jobId}`);
            this.jobId = job.jobId;
            this.title = job.title;
            this.description = job.description;
            this.descriptionPdfLink = job.descriptionPdfLink;
            this.jobSkills = job.jobSkills;
            this.loadContent('job');
        } else {
            this.loadContent('nojob');
        }
    };

    loadJobStep2 = async (jobRef, applicationId) => {
        this.jobRef = this.getParameterByName(jobRef);
        this.applicationId = this.getParameterByName(applicationId)
        console.log(`Trying to fetch job with id ${this.jobRef}`);
        const response = await fetch(`http://localhost:7070/job/2/${this.jobRef}`);
        if (response.status === 200) {
            const job = await response.json();
            console.log(`Fetched job with id ${job.jobId}`);
            this.jobId = job.jobId;
            this.title = job.title;
            this.description = job.description;
            this.description = job.descriptionPdfLink;
            this.jobSkills = job.jobSkills;
            this.loadContent('jobstep2');
        } else {
            console.log(`No job found`);
            this.loadContent('nojob');
        }
    };

    getParameterByName(name) {
        console.log(`Try to get parameter for name ${name}`);
        const url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    loadContent = (uri) => {
        const contentUri = `${uri}.html`
        fetch(contentUri)
            .then(r => {
                if (r.status === 200) {
                    return r.text()
                } else {
                    return '...j';
                }
            })
            .then(content => this.updateSlot(content));
    };

    updateSlot = (content) => {
        this.jobSlot = document.querySelector('#jobslot');
        this.jobSlot.insertAdjacentHTML('beforeend', content);
        const elJobIntro1 = this.jobSlot.querySelector('#jobIntro1');
        if (elJobIntro1) {
            elJobIntro1.textContent = 'Herzlich Willkommen auf der Bewerbungsseite von';
        }
        const elJobIntroCompany = this.jobSlot.querySelector('#jobIntroCompany');
        if (elJobIntroCompany) {
         elJobIntroCompany.textContent = 'Andreas Schmidt Sales & Consulting';
        }
        const elJobIntro2 = this.jobSlot.querySelector('#jobIntro2');
        if (elJobIntro2) {
            elJobIntro2.textContent = 'für die Position';
        }
        const elJobIntroPosition = this.jobSlot.querySelector('#jobIntroPosition');
        if (elJobIntroPosition) {
            elJobIntroPosition.textContent = this.title;
        }
        const elJobDescription = this.jobSlot.querySelector('#jobDescription');
        const elJobDescriptionPdfLink = this.jobSlot.querySelector('#jobDescriptionPdfLink');
        const elPdfCanvas = this.jobSlot.querySelector('#pdfCanvas');
        if (this.description) {
            if (elJobDescription) {
                elJobDescription.textContent = this.description;
            }
            if (elJobDescriptionPdfLink) {
                elJobDescriptionPdfLink.style.visibility = "hidden";
            }
            if (elPdfCanvas) {
                elPdfCanvas.style.visibility = "hidden";
            }
        } else if (this.descriptionPdfLink) {
            console.log("This is the descriptionPDFLink:", this.descriptionPdfLink)
            const newJobDescriptionLink = document.createElement('a');
            newJobDescriptionLink.setAttribute('href',this.descriptionPdfLink);
            newJobDescriptionLink.innerHTML = "Beschreibung";
            elJobDescriptionPdfLink.appendChild(newJobDescriptionLink);
            if (elJobDescription) {
                elJobDescription.style.visibility = "hidden";
            }
            this.previewPdf(this.descriptionPdfLink)
        }
        const applyDiv = document.querySelector('#applyDiv')
        if (applyDiv) {
            applyDiv.style.display = "block";
        }
        const applyDivInner = document.querySelector('#applyDivInner')
        if (applyDivInner) {
            applyDivInner.style.display = "block";
        }
        this.setRequirementsList();
        this.setRequirementsTable();
        this.setRequirementsTableWithAnswer();
    };

    setRequirementsList = () => {
        const elRequirementsList = this.jobSlot.querySelector('#requirementsList');
        if (!elRequirementsList) {
            return;
        }
        this.jobSkills.forEach(jobSkill => {
            const li = document.createElement("li");
            let text = jobSkill.skillTitle + " " + jobSkill.mandatory + " " + jobSkill.weightage + "% " + jobSkill.yearsOfExperience;
            li.appendChild(document.createTextNode(text));
            elRequirementsList.appendChild(li)
        });
    }

    setRequirementsTable = () => {
        const reqTable = document.querySelector('#requirementstable');
        if (!reqTable) {
            return;
        }
        this.fillSkillsTable(reqTable, false);
    }

    setRequirementsTableWithAnswer = () => {
        const reqTable = document.querySelector('#requirementstablewithanswer');
        if (!reqTable) {
            return;
        }
        this.fillSkillsTable(reqTable, true);
    }

    previewPdf(pdfLink) {
        var url = pdfLink + '?origin='+ window.location.host;
        // Loaded via <script> tag, create shortcut to access PDF.js exports.
        var pdfjsLib = window['pdfjs-dist/build/pdf'];
        // The workerSrc property shall be specified.
        pdfjsLib.GlobalWorkerOptions.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';
        // Asynchronous download of PDF
        var loadingTask = pdfjsLib.getDocument(url);
        loadingTask.promise.then(function(pdf) {
            // Fetch the first page
            var pageNumber = 1;
            pdf.getPage(pageNumber).then(function(page) {
                console.log('Page loaded');
                var scale = 1.5;
                var viewport = page.getViewport({scale: scale});
                // Prepare canvas using PDF page dimensions
                var canvas = document.getElementById('pdfCanvas');
                var context = canvas.getContext('2d');
                canvas.height = viewport.height;
                canvas.width = viewport.width;
                // Render PDF page into canvas context
                var renderContext = {
                    canvasContext: context,
                    viewport: viewport
                };
                var renderTask = page.render(renderContext);
                renderTask.promise.then(function () {
                    console.log('Page rendered');
                });
            });
        }, function (reason) {
            // PDF loading error
            console.error(reason);
        });
    }

    fillSkillsTable(reqTable, withAnswer) {
        const tBodyElement = document.createElement("tbody");
        reqTable.appendChild(tBodyElement);
        this.jobSkills.forEach((jobSkill, index) => {
            const counter = index + 1;
            const trElement = document.createElement("tr");

            const tdElementSkillNr = document.createElement("td");
            tdElementSkillNr.appendChild(document.createTextNode(counter));
            trElement.appendChild(tdElementSkillNr);

            const tdElementSkillTitle = document.createElement("td");
            tdElementSkillTitle.appendChild(document.createTextNode(jobSkill.skillTitle));
            trElement.appendChild(tdElementSkillTitle);

            const tdElementSkillMandatory = document.createElement("td");
            let skillText = '';
            if (jobSkill.mandatory || jobSkill.mandatory === 'true') {
                skillText = 'x';
            }
            tdElementSkillMandatory.appendChild(document.createTextNode(skillText));
            trElement.appendChild(tdElementSkillMandatory);

            if (withAnswer) {
                const tdElementSkillMandatoryAnswer = document.createElement("td");
                const mandatoryAnswerInput = document.createElement("input");
                mandatoryAnswerInput.type = "checkbox";
                mandatoryAnswerInput.name = 'skillreq' + counter;
                tdElementSkillMandatoryAnswer.appendChild(mandatoryAnswerInput);
                trElement.appendChild(tdElementSkillMandatoryAnswer);
            }

            const tdElementSkillWeightage = document.createElement("td");
            tdElementSkillWeightage.appendChild(document.createTextNode(jobSkill.weightage));
            trElement.appendChild(tdElementSkillWeightage);

            const tdElementSkillYearsOfExperience = document.createElement("td");
            tdElementSkillYearsOfExperience.appendChild(document.createTextNode(jobSkill.yearsOfExperience));
            trElement.appendChild(tdElementSkillYearsOfExperience);

            if (withAnswer) {
                const tdElementExperienceAnswer = document.createElement("td");
                const experienceAnswerInput = document.createElement("input");
                experienceAnswerInput.type = "text";
                experienceAnswerInput.maxLength = 2;
                experienceAnswerInput.minLength = 1;
                experienceAnswerInput.size = 3;
                experienceAnswerInput.name = 'skillexp' + counter;
                experienceAnswerInput.required = true;
                this.setInputFilter(experienceAnswerInput, function(value) {
                    return /^\d*?\d*$/.test(value); // Allow digits and '.' only, using a RegExp
                });
                tdElementExperienceAnswer.appendChild(experienceAnswerInput);
                trElement.appendChild(tdElementExperienceAnswer);
            }

            tBodyElement.appendChild(trElement);
        });
    }

    sendRequirementsAnswer = async () => {
        const form = document.querySelector('#requirementsapply');
        let skillreq1 = false;
        if (form.elements["skillreq1"]) {
            skillreq1 = form.elements["skillreq1"].checked;
        }
        console.log(`SkilLReq1 ${skillreq1}`);
        let skillreq2 = false;
        if (form.elements["skillreq2"]) {
            skillreq2 = form.elements["skillreq2"].checked;
        }
        console.log(`SkilLReq2 ${skillreq2}`);
        let skillreq3 = false;
        if (form.elements["skillreq3"]) {
            skillreq3 = form.elements["skillreq3"].value === 'on';
        }
        let skillreq4 = false;
        if (form.elements["skillreq4"]) {
            skillreq4 = form.elements["skillreq4"].value === 'on';
        }
        let skillreq5 = false;
        if (form.elements["skillreq5"]) {
            skillreq5 = form.elements["skillreq5"].value === 'on';
        }
        let skillreq6 = false;
        if (form.elements["skillreq6"]) {
            skillreq6 = form.elements["skillreq6"].value === 'on';
        }
        let skillreq7 = false;
        if (form.elements["skillreq7"]) {
            skillreq7 = form.elements["skillreq7"].value === 'on';
        }
        let skillreq8 = false;
        if (form.elements["skillreq8"]) {
            skillreq8 = form.elements["skillreq8"].value === 'on';
        }
        let skillreq9 = false;
        if (form.elements["skillreq9"]) {
            skillreq9 = form.elements["skillreq9"].value === 'on';
        }
        let skillreq10 = false;
        if (form.elements["skillreq10"]) {
            skillreq10 = form.elements["skillreq10"].value === 'on';
        }
        let skillexp1 = "";
        if (form.elements["skillexp1"]) {
            skillexp1 = form.elements["skillexp1"].value;
        }
        let skillexp2 = "";
        if (form.elements["skillexp2"]) {
            skillexp2 = form.elements["skillexp2"].value;
        }
        let skillexp3 = "";
        if (form.elements["skillexp3"]) {
            skillexp3 = form.elements["skillexp3"].value;
        }
        let skillexp4 = "";
        if (form.elements["skillexp4"]) {
            skillexp4 = form.elements["skillexp4"].value;
        }
        let skillexp5 = "";
        if (form.elements["skillexp5"]) {
            skillexp5 = form.elements["skillexp5"].value;
        }
        let skillexp6 = "";
        if (form.elements["skillexp6"]) {
            skillexp6 = form.elements["skillexp6"].value;
        }
        let skillexp7 = "";
        if (form.elements["skillexp7"]) {
            skillexp7 = form.elements["skillexp7"].value;
        }
        let skillexp8 = "";
        if (form.elements["skillexp8"]) {
            skillexp8 = form.elements["skillexp8"].value;
        }
        let skillexp9 = "";
        if (form.elements["skillexp9"]) {
            skillexp9 = form.elements["skillexp9"].value;
        }
        let skillexp10 = "";
        if (form.elements["skillexp10"]) {
            skillexp10 = form.elements["skillexp10"].value;
        }

        const dataJson = {
            "skillreq1": skillreq1,
            "skillreq2": skillreq2,
            "skillreq3": skillreq3,
            "skillreq4": skillreq4,
            "skillreq5": skillreq5,
            "skillreq6": skillreq6,
            "skillreq7": skillreq7,
            "skillreq8": skillreq8,
            "skillreq9": skillreq9,
            "skillreq10": skillreq10,
            "skillexp1": skillexp1,
            "skillexp2": skillexp2,
            "skillexp3": skillexp3,
            "skillexp4": skillexp4,
            "skillexp5": skillexp5,
            "skillexp6": skillexp6,
            "skillexp7": skillexp7,
            "skillexp8": skillexp8,
            "skillexp9": skillexp9,
            "skillexp10": skillexp10
        };
        await fetch(`http://localhost:7070/application/skills?jobid=${this.jobRef}&applicationid=${this.applicationId}`, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(dataJson)
        }).then(r => {
            console.log("Got a response");
            if (r.status === 200 || r.status === 201) {
                console.log("Got a response with 200");
                return r.text()
            } else {
                return '...';
            }
        }).then(content => {
            router.loadContent("personaldata");
        });
    }

    sendPersonalData = async () => {
        const form = document.querySelector('#personalDataForm');
        this.salutation = form.elements["salutation"].value;
        if (!this.salutation || this.salutation.length < 1) {
            alert("Bitte Anrede auswählen!");
            return;
        }
        const firstName = form.elements["firstName"].value;
        this.lastName = form.elements["lastName"].value;
        const email = form.elements["email"].value;
        // const telephone = form.elements["telephone"].value;
        const dataJson = {
            "salutation": this.salutation,
            "firstName": firstName,
            "lastName": this.lastName,
            "email": email
            // "telephone": telephone
        };
        await fetch(`http://localhost:7070/application/personalData?jobid=${this.jobRef}&applicationid=${this.applicationId}`, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(dataJson)
        }).then(r => {
            console.log("Got a response");
            if (r.status === 200 || r.status === 201) {
                return r.text()
            } else {
                return '...';
            }
        }).then(content => {
            router.loadContent("thankyou");
            const jobSlot = document.querySelector('#jobslot');
            if (jobSlot) {
                jobSlot.innerHTML = '';
            }
            const cancelLink = document.querySelector('#cancel');
            console.log("Going to remove cancel link");
            cancelLink.parentElement.removeChild(cancelLink);
            const reloadLink = document.querySelector('#reload');
            if (reloadLink) {
                console.log("Going to remove reloadLink");
                reloadLink.parentElement.removeChild(reloadLink);
            }
        });
    }

    setInputFilter(textbox, inputFilter) {
        ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
            textbox.addEventListener(event, function() {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
            });
        });
    }

    setGdpr(value) {
        this.gdprRef = value;
    }

}

const job = new Job();