class IndexView {
    constructor() {
        window.addEventListener("hashchange", e => this.onRouteChange(e));
        this.slot = document.querySelector('#slot');
        window.addEventListener("DOMSubtreeModified", e => {
            const jobRefHiddenParam = document.querySelector('#jobRef');
            if (jobRefHiddenParam && job) {
                jobRefHiddenParam.value = job.jobRef;
            }
            const gdprRefHiddenParam = document.querySelector('#gdprRef');
            if (gdprRefHiddenParam && job) {
                gdprRefHiddenParam.value = job.gdprRef;
            }
        })
    }

    onRouteChange(e) {
        let hashLocation = window.location.hash.substring(1);
        if (hashLocation && hashLocation!=='') {
            console.log(`Updating for hashLocation ${hashLocation}`);
            this.loadContent(hashLocation);
        } else {
            window.location.href = e.newURL;
        }
    }

    loadContent(uri) {
        const contentUri = `${uri}.html`
        fetch(contentUri)
            .then(r => {
                if (r.status === 200) {
                    return r.text()
                } else {
                    return '';
                }
            })
            .then(content => {
                this.updateSlot(content);
                let jobSlotElement = document.querySelector('#jobslot');
                if (uri === 'apply') {
                    let applyElement = document.querySelector('#applyLink');
                    if (applyElement) {
                        applyElement.style.visibility = "hidden";
                    }
                    let applyInnerElement = document.querySelector('#applyLinkInner');
                    if (applyInnerElement) {
                        applyInnerElement.style.visibility = "hidden";
                    }
                } else if (uri === 'gdpr') {
                    let gdprElement = document.querySelector('#gdprLink');
                    if (gdprElement) {
                        gdprElement.style.visibility = "hidden";
                    }
                    let gdprInnerElement = document.querySelector('#gdprLinkInner');
                    if (gdprInnerElement) {
                        gdprInnerElement.style.visibility = "hidden";
                    }
                    if (jobSlotElement) {
                        jobSlotElement.remove();
                    }
                } else if ( uri === 'personaldata') {
                    if (jobSlotElement) {
                        jobSlotElement.remove();
                    }
                } else {
                    let gdprElement = document.querySelector('#gdprLink');
                    if (gdprElement) {
                        gdprElement.style.visibility = "visible";
                    }
                    let gdprInnerElement = document.querySelector('#gdprLinkInner');
                    if (gdprInnerElement) {
                        gdprInnerElement.style.visibility = "visible";
                    }
                }
            });
    }

    updateSlot(content) {
        this.slot.innerHTML = content;
        const documentsIntro = this.slot.querySelector('#documentsIntro');
        if (documentsIntro !== undefined && documentsIntro !== null) {
            documentsIntro.textContent = 'Bitte laden Sie nun für die Position ' + job.title + ' Ihre Unterlagen in den jeweiligen Rubriken hoch.\n'
        }
        const thankYouGreeting = document.querySelector('#thankYouGreeting');
        if (job.salutation === 'MRS') {
            thankYouGreeting.textContent = 'Sehr geehrte Frau ' + job.lastName + ',';
        } else if (job.salutation === 'MR') {
            thankYouGreeting.textContent = 'Sehr geehrter Herr ' + job.lastName + ',';
        } else if (job.salutation === 'DIV') {
            thankYouGreeting.textContent = 'Sehr geehrt* ' + job.lastName + ',';
        }
    }
}

const router = new IndexView();
